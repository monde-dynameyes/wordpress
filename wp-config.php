<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'admin_wordpress');

/** MySQL database password */
define('DB_PASSWORD', '4aY!sWafsTh]6~RZ\vT42,Bb8#{ZMc\)');

/** MySQL hostname */
define('DB_HOST', 'sanctuary:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9qwckXjN TjU+Ti5PVx~:Wz7aFkINnJFnEY?-)ZMx/<D vSP9/i;SaGV+=AJJ+-`');
define('SECURE_AUTH_KEY',  'd/MKH#xD?O6@[5{;]KtYq$t17l~[Vv91ZbX]:g_5]{bVJ1&JbJFi3)QUm[u&]d[n');
define('LOGGED_IN_KEY',    '(PXOqJ1v,kvp]y&R>>x.F{hzs6tFVmXG??2L+V.-TY2t#ESuw|W$|jiZ2p**UPug');
define('NONCE_KEY',        'v$Oo)<F@J^UQV+~C5)a7;x&SwrX5vwGg/r9NSt^/&ZH^{i(R|5JWm?]+mssLVSxR');
define('AUTH_SALT',        '^&==A-V{#K(7u$H@ItX8oS{n/>#S.5{L{fl6&&o]2R;QbLhr6XZ24$TrF@My]wQ4');
define('SECURE_AUTH_SALT', 'ZE5qU^aYlC<}JGCz7uzxH{.:n_ARQi7w07`SWr3sh&LGKEB%ybDjgNcU0Wz:@0oE');
define('LOGGED_IN_SALT',   '$2ZMPdUS}y]-_*F=/<CF&t.)W2qgNs*OC1W(g9lJGKVzh[}hQ)$U#9!4rl{yc:wZ');
define('NONCE_SALT',       'U#f@wx39_SDFE~V<HH8@$Nf_~o5!@<K5g29]eU[s>6l:oK&SawbBa.?dhSKnmH62');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


/** Cannot save plugins to localhost due to WordPress uses by default only a ftp server */
define('FS_METHOD', 'direct');
