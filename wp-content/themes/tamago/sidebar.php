<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tamago
 */

if ( ! is_active_sidebar( 'right-sidebar' ) ) {
	return;
}
?>


<?php if ( is_home() || is_page('Static Page') ) :
	?>
	<aside id="secondary" class="right-sidebar">
	 <?php dynamic_sidebar( 'right-sidebar' ); ?>
	</aside><!-- #secondary -->
<?php else : ?>
	<!-- Category query -->
		<h2>Posts related:</h2>
		<?php
		global $post;
			//get the categories a post belongs to
			$cats = get_the_category($post->ID);

			$cat_array = array();
			    foreach($cats as $key1 => $cat) {
			        $cat_array[] = $cat->name;
			    }
					// echo "<pre>"; print_r($cat_array);
			//get the tags a post belongs to
			$tags = get_the_tags($post->ID);

			$tag_array = array();
			    foreach($tags as $key2 => $tag) {
			        $tag_array[] = $tag->term_id;
			    }
					//echo "<pre>"; print_r($tag_array);
				$args = array(
		    'posts_per_page' => 5,
		    'tax_query' => array(
		        'relation' => 'OR',
		        array(
		            'taxonomy' => 'category',
		            'field' => 'slug',
		            'terms' => $cat_array,
		            'include_children' => false
		        ),
		        array(
		            'taxonomy' => 'post_tag',
		            'field' => 'slug',
		            'terms' => $tag_array,
		        )
		    )
		);

		$the_query = new WP_Query( $args );
			while($the_query->have_posts() ) :
				$the_query->the_post();
				// echo "<pre>"; print_r(the_title());
		 ?>
		 <ul>
		 	<li>
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</li>
		 </ul>
	 <?php endwhile; ?>
<?php endif; ?>
