<?php
/**
 * Template Name: All Category
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tamago
 */

get_header();
?>

<div id="primary" class="content-area col-md-8">
  <main id="main" class="site-main">

    <?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<!-- Condition goes here for specific Categories -->
    <?php
      if ( is_page('Kpop') ):
        $cat_query = new WP_Query( array(
            'posts_per_page' => 10,
            'category_name' => 'kpop', //category-slug
        ) );

        if ( $cat_query->have_posts()) :

          while ( $cat_query->have_posts() ) :
            $cat_query->the_post();
            ?>
              <div class="entry-content">
                <?php the_content();?>
              </div>
              <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>
              <span class="cat-links">Posted in: <?php the_category( ', ' ) ?></span>
              <span class="tags-links"> <?php the_tags(); ?></span>
              <hr>
            </article>
            <?php
          endwhile;
        endif;
      elseif (is_page('Anime')):
        $cat_query = new WP_Query( array(
            'posts_per_page' => 10,
            'category_name' => 'anime', //category-slug
        ) );
        if ( $cat_query->have_posts()) :

          while ( $cat_query->have_posts() ) :
            $cat_query->the_post();
            ?>
              <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>
              <div class="entry-content">
                <?php the_content();?>
              </div>
              <span class="cat-links">Posted in: <?php the_category( ', ' ) ?></span>
              <span class="tags-links"> <?php the_tags(); ?></span>
              <hr>
            </article>
            <?php
          endwhile;
        endif;
      elseif (is_page('Music')):
        $cat_query = new WP_Query( array(
            'posts_per_page' => 10,
            'category_name' => 'music', //category-slug
        ) );
        if ( $cat_query->have_posts()) :

          while ( $cat_query->have_posts() ) :
            $cat_query->the_post();
            ?>
              <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>
              <div class="entry-content">
                <?php the_content();?>
              </div>
              <span class="cat-links">Posted in: <?php the_category( ', ' ) ?></span>
              <span class="tags-links"> <?php the_tags(); ?></span>
              <hr>
            </article>
            <?php
          endwhile;
        endif;
       elseif (is_page('Games')) :
         $cat_query = new WP_Query( array(
             'posts_per_page' => 10,
             'category_name' => 'games', //category-slug
         ) );
         if ( $cat_query->have_posts()) :

           while ( $cat_query->have_posts() ) :
             $cat_query->the_post();
             ?>
               <header class="entry-header">
                 <h1 class="entry-title"><?php the_title(); ?></h1>
               </header>
               <div class="entry-content">
                 <?php the_content();?>
               </div>
               <span class="cat-links">Posted in: <?php the_category( ', ' ) ?></span>
               <span class="tags-links"> <?php the_tags(); ?></span>
               <hr>
             </article>
             <?php
           endwhile;
       endif;
       elseif (is_page('Food')) :
         $cat_query = new WP_Query( array(
             'posts_per_page' => 10,
             'category_name' => 'food', //category-slug
         ) );
         if ( $cat_query->have_posts()) :

           while ( $cat_query->have_posts() ) :
             $cat_query->the_post();
             ?>
               <header class="entry-header">
                 <h1 class="entry-title"><?php the_title(); ?></h1>
               </header>
               <div class="entry-content">
                 <?php the_content();?>
               </div>
               <span class="cat-links">Posted in: <?php the_category( ', ' ) ?></span>
               <span class="tags-links"> <?php the_tags(); ?></span>
               <hr>
             </article>
             <?php
           endwhile;
       endif;
      endif;
     ?>

    <!--- Querying all different Categories ends here --->
		</main><!-- .site-main -->
	</div><!-- .content-area -->
  <div class="col-md-4">
    <?php get_sidebar('custom'); ?>
  </div>
  <?php
  get_footer();
