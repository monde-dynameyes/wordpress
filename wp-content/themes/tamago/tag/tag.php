<?php
/**
 * Template Name: All Tags
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tamago
 */

get_header();
?>

<div id="primary" class="content-area col-md-8">
  <main id="main" class="site-main">

    <?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<!-- Condition goes here for specific Categories -->
    <?php
      if ( is_page('momoland') ):
        $tag_query = new WP_Query( array(
            'posts_per_page' => 10,
            'tag' => 'momoland', //tag-slug
        ) );

        if ( $tag_query->have_posts()) :

          while ( $tag_query->have_posts() ) :
            $tag_query->the_post();
            ?>
              <div class="entry-content">
                <?php the_content();?>
              </div>
              <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>
              <hr>
            </article>
            <?php
          endwhile;
        endif;
      elseif (is_page('twice')):
        $tag_query = new WP_Query( array(
            'posts_per_page' => 10,
            'tag' => 'twice', //tag-slug
        ) );
        if ( $tag_query->have_posts()) :

          while ( $tag_query->have_posts() ) :
            $tag_query->the_post();
            ?>
              <header class="entry-header">
                <h1 class="entry-title"><?php the_title(); ?></h1>
              </header>
              <div class="entry-content">
                <?php the_content();?>
              </div>
              <hr>
            </article>
            <?php
          endwhile;
        endif;
      endif;
     ?>

    <!--- Querying all different Categories ends here --->
		</main><!-- .site-main -->
	</div><!-- .content-area -->
  <?php
  get_footer();
